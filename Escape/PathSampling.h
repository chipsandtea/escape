#ifndef ESCAPE_PATHSAMPLING_H_
#define ESCAPE_PATHSAMPLING_H_
#include <cstdlib>
#include <iomanip>
#include "GetAllCounts.h"
#include "Graph.h"

using namespace Escape;

double* KIndices(VertexIdx k){ //Chooses K random numbers in range (0,1)
	double* indices;
	indices = new double[k];
	for(VertexIdx i=0;i<k;i++){
		VertexIdx rand_x = rand();
		indices[i]=((double)((double)(rand_x)/((double)(RAND_MAX))));
	}
	std::sort(indices,indices+k); //indices is a sorted list of k random numbers in (0,1), to be
								  //translated into a list of k random vertices
	return indices;
}

//Chooses k random vertices with probabilities given by the CGraph's CDF as given by 
//p_v=(d_v choose 2)/W
VertexIdx* ChooseKRandom(CGraph *cg, VertexIdx k){
	VertexIdx W=0; //Number of wedges
	VertexIdx num_of_vertices = (long)cg->nVertices; //number of vertices
	VertexIdx Wv[num_of_vertices]; //Wv[i]=total number of wedges centered at vertex v
	for(VertexIdx i=0;i<num_of_vertices;i++){
		VertexIdx degree = cg->degree(i); //degree=number of edges connected to v
		Wv[i]=(int)((degree)*(degree-1)/2); //Wv[i]=(degree choose 2)=number of wedges at vertex i
		W=W+Wv[i]; //Increase number of total wedges and iterate over all vertices to get total wedges
	}
	double CDF [num_of_vertices]; //initialize the CDF array for the graph cg
	double PMF [num_of_vertices]; //initialize the PMF array for the graph cg
	PMF[0]=((double)(Wv[0]))/((double)(W)); //Sets PMF[0]=number of wedges at 0 divided by total number of wedges
	CDF[0]=PMF[0]; //Initializes CDF[0]=PMF[0];

	//Fills out PMF and CDF arrays
	for(VertexIdx i=1;i<num_of_vertices;i++){
		PMF[i]=((double)(Wv[i]))/((double)(W));
		CDF[i]=CDF[i-1]+PMF[i];

	}
	double *indices;
	indices = KIndices(k);

	VertexIdx RandomVertices [k]; //RandomVertices will be the list of the k random vertices
	//current_index is the index of indices (and thus RandomVertices) currently being worked on
	VertexIdx start_iteration=0; 
	//start_iteration is the index that the following for loop starts from
	//(note that since indices is sorted, the next vertex can only be later in the list)
	
	//Picks k vertices based on the CDF and stores it in RandomVertices
	int counter = 0;
	// EdgeIdx bool1 = 0;
	// EdgeIdx bool2 = 0;
	for(VertexIdx i=0; i<k; i++){

		for(VertexIdx j=start_iteration; j < num_of_vertices-1; j++){
			// ARE WE SURE WE INITIALIZE ALL VALUES OF CDF?
			// Conditional jump or move depends on uninitialised value(s)
			// where CDF[j] comes up?
			 
			
			/*
			bool1 = cg->getEdge(i, j);
			bool2 = cg->getEdge(j, i);
			printf("i = %d, j = %d, isedge i->j = %d\n", i,j,bool1);
			printf("i = %d, j = %d, isedge j->i= %d\n", i,j,bool2);
			if(bool1 != -1){
				VertexIdx v1 = cg->nbors[bool1];
				VertexIdx v2 = cg->nbors[bool2];
				printf("v1 = %d\n", v1);
				printf("v2 = %d\n", v2);
			}
			printf("====\n");
			*/
			if(j != 0 && CDF[j] == CDF[j-1]){
				continue;
			}
			
			if(indices[i]<CDF[0]){
				RandomVertices[i]=0;
				counter++;
				start_iteration=0;
				break;
			}
			if(CDF[j]<indices[i] && indices[i]<CDF[j+1]){
				counter++;
				RandomVertices[i]=j+1;
				start_iteration=j;
				break;
			}
		}
	}

	return RandomVertices;
}
float WedgeSample(CGraph *cg, int k) // k = number of random vertices selected
{

	cg->sortById();

	VertexIdx *RandomVertices;
	// pass pointer of random vertices?
	RandomVertices =  ChooseKRandom(cg, k); //Choose k random vertices and store in an array
	
	VertexIdx closed = 0;
	VertexIdx total = 0;

	VertexIdx v1;
	VertexIdx v2;

	for(VertexIdx i=0;i<k;i++){
		VertexIdx M = cg->degree(RandomVertices[i]); //Degree of vertex at index RandomVertices[i]

		if(M==1){
			continue; //only one neighbor
		}
		int rand_a = rand();
		int rand_b = rand();
		v1=cg->nbors[cg->offsets[ RandomVertices[i] ]+(rand_a % M)];
		v2=cg->nbors[cg->offsets[ RandomVertices[i] ]+(rand_b % M)]; 

		// if(v1 > num_of_vertices){
		// 	v1=cg->nbors[cg->offsets[ RandomVertices[i] ]+(rand_a % M)];
		// }
		// if(v2 > num_of_vertices){
		// 	v2=cg->nbors[cg->offsets[ RandomVertices[i] ]+(rand_b % M)];
		// }
		
		while(v1 == v2){
			int rand_x = rand();
			v2=cg->nbors[cg->offsets[ RandomVertices[i] ]+(rand_x % M)];
			
		}

		if(cg->isEdgeBinary(v1,v2)){
			closed++;
		}
		total++;
	}
	return ((float)closed/(float)total);
}
#endif
