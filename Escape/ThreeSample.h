#ifndef ESCAPE_THREESAMPLE_H
#define ESCAPE_THREESAMPLE_H
#include <cstdlib>
#include <iomanip>
#include "GetAllCounts.h"
#include "PathSampling.h"

using namespace Escape;

EdgeIdx*** threeEdgeSample(CGraph *cg, EdgeIdx k){
	//... 'Sample' Algorithm	

	EdgeIdx num_of_vertices = (long)cg->nVertices; //Te is Tau_{e}: for each edge (u,v), it's (d_u-1)(d_v-1) 
	EdgeIdx **Te = new EdgeIdx*[num_of_vertices];
	for(int size=0;size<num_of_vertices;size++){
		Te[size]= new EdgeIdx[num_of_vertices];
	}
	EdgeIdx W=0; //W=Sum of all T_{e}
	//Compute T_{e} for all edges
	for(VertexIdx i=0;i<num_of_vertices;i++){
		for(VertexIdx j=0;j<num_of_vertices;j++){
			Te[i][j]=-1;
		}
	}
	for(VertexIdx i=0;i<num_of_vertices;i++){
		VertexIdx degree = cg->degree(i);
		for(VertexIdx d=0;d<degree;d++){
			VertexIdx j=cg->nbors[(cg->offsets[i])+d];
			if(cg->isEdgeBinary(i,j) && i<j){
				Te[i][j]=(degree-1)*((cg->degree(j))-1);
				W+=Te[i][j];
			}
			else{
				Te[i][j]=-1;
			}
		}
	}
	// printf("\nW=%ld\n",W);
	//double PePMF [num_of_vertices][num_of_vertices]; // PMF of probability of e being picked for a sample
	// CDF of probability of e being picked for a sample
	double **PeCDF = new double*[num_of_vertices];
	for(int size=0;size<num_of_vertices;size++){
		PeCDF[size]=new double[num_of_vertices];
	}
	double runningTot =0;

	for(VertexIdx i=0;i<num_of_vertices-1;i++){ 		//for all vertices
		for(VertexIdx j=i+1;j<num_of_vertices;j++){ 	//check it's edges
			if(Te[i][j]!=-1){							//if it exists, calculate PMF and CDF
				//PePMF[i][j]=((double)Te[i][j])/((double)W);
				PeCDF[i][j]=runningTot+((double)Te[i][j])/((double)W);
				//printf("\ni=%d, j=%d, PeCDF[%d][%d]=%f",i,j,i,j,PeCDF[i][j]);
				runningTot+=((double)Te[i][j])/((double)W);
			}
			else{
				// PeCMF[i][j]=-1;
				PeCDF[i][j]=-1;
			}
		}
	}

	// for(int i=0;i<num_of_vertices;i++){
	// 	for(int j=0;j<num_of_vertices;j++){
	// 		if(PeCDF[i][j]!=-1){
	// 			printf(" %f ",PeCDF[i][j]);
	// 			printf("\n");	
	// 		}
			
	// 	}
	// }
	double *indices;
	indices=KIndices(k); //k random numbers in range (0,1) to pick k edge samples
	//declare sampledEdges

	EdgeIdx ***sampledEdges;
	sampledEdges = new EdgeIdx**[k];
	for(int i = 0; i < k; ++i){
		sampledEdges[i] = new EdgeIdx*[3];

		for(int j = 0; j < 3; ++j){
			sampledEdges[i][j] = new EdgeIdx[2];
		}
	}
	bool breakflag=false;
	//k times: iterate downward checking decreasing probabilities. the first one you find that has CDF less than the random number chosen
	//is chosen to be our edge (u,v)
	int t=0; //t is the index of the current set of edges being sampled
	for(int i=0;i<num_of_vertices-1;i++){
		for(int j=i+1;j<num_of_vertices;j++){
			if(PeCDF[i][j]>indices[t] && PeCDF[i][j]>0){
				// printf("\ni=%d, j=%d, PeCDF[%d][%d]=%f",i,j,i,j,PeCDF[i][j]);
				// printf("\nindices[%d]=%f\n",t,indices[t]);
				//printf("\nindices[%d]=%f, PeCDF[%d][%d]=%f",t,indices[t],i,j,PeCDF[i][j]);
				sampledEdges[t][1][0]=i;
				sampledEdges[t][1][1]=j;
				t++;
			}
			if(t>=k) breakflag=true; break;
		}
		if(breakflag) break;
	}

	//printf("\nhi!\n");
	//now to find the other 2 edges relevant to the sample algorithm
	for(VertexIdx i=0;i<k;i++){
		VertexIdx u=sampledEdges[i][1][0];
		VertexIdx v=sampledEdges[i][1][1];

		VertexIdx degu=cg->degree(u);
		VertexIdx degv=cg->degree(v);

		VertexIdx uprime=v;
		VertexIdx vprime=u;
		while(uprime==v){
			int rand_x=rand();
			uprime = cg->nbors[(cg->offsets[u])+(rand_x%degu)];
		}
		while(vprime==u){
			int rand_y=rand();
			vprime = cg->nbors[(cg->offsets[v])+(rand_y%degv)];
		}
		sampledEdges[i][0][0] = uprime;
		sampledEdges[i][0][1] = u;
		sampledEdges[i][2][0] = v;
		sampledEdges[i][2][1] = vprime;
	}
	return sampledEdges;

}

//This function fills out C1-hat, C2-hat, C3-hat, C4-hat, C5-hat, C6-hat

void threePathSample(CGraph *cg, CDAG *dag, double (&fourCounts)[6], EdgeIdx k){
	EdgeIdx ***sampledEdges;
	sampledEdges = new EdgeIdx**[k];
	for(int i = 0; i < k; ++i){
		sampledEdges[i] = new EdgeIdx*[3];

		for(int j = 0; j < 3; ++j){
			sampledEdges[i][j] = new EdgeIdx[2];
		}
	}
	sampledEdges = threeEdgeSample(cg,k);

	//this part here is duplicate code...fix later
	EdgeIdx num_of_vertices = (long)cg->nVertices; //Te is Tau_{e}: for each edge (u,v), it's (d_u-1)(d_v-1) 
	EdgeIdx **Te = new EdgeIdx*[num_of_vertices];
	for(int size=0;size<num_of_vertices;size++){
		Te[size]= new EdgeIdx[num_of_vertices];
	}
	EdgeIdx W=0; 
	for(VertexIdx i=0;i<num_of_vertices;i++){
		for(VertexIdx j=0;j<num_of_vertices;j++){
			Te[i][j]=-1;
		}
	}
	for(VertexIdx i=0;i<num_of_vertices;i++){
		VertexIdx degree = cg->degree(i);
		for(VertexIdx d=0;d<degree;d++){
			VertexIdx j=cg->nbors[(cg->offsets[i])+d];
			if(cg->isEdgeBinary(i,j) && i<j){
				Te[i][j]=(degree-1)*((cg->degree(j))-1);
				W+=Te[i][j];
			}
			else{
				Te[i][j]=-1;
			}
		}
	}


	int count[6];
	for(int i=0;i<6;i++){
		count[i]=0;
	}

	for(int i=0;i<k;i++){

		bool uPrimevPrime=false;
		bool uPrimev=false;
		bool vPrimeu=false;

		VertexIdx u = sampledEdges[i][1][0];
		VertexIdx v = sampledEdges[i][1][1];
		VertexIdx uprime = sampledEdges[i][0][0];
		VertexIdx vprime = sampledEdges[i][2][1];

		if(cg->isEdgeBinary(uprime,vprime)){
			uPrimevPrime=true;
		}
		if(cg->isEdgeBinary(uprime,v)){
			uPrimev=true;
		}
		if(cg->isEdgeBinary(u,vprime)){
			vPrimeu=true;
		}


		if(uPrimevPrime && uPrimev && vPrimeu){
			count[5]++;
			continue;
		}

		if((uPrimevPrime && uPrimev && !vPrimeu) || (uPrimevPrime && vPrimeu && !uPrimev)){
			count[4]++;
			continue;
		}
		if(uPrimevPrime && !(uPrimev || vPrimeu)){
			count[3]++;
			continue;
		}
		if((uPrimev || vPrimeu) && !uPrimevPrime){
			count[2]++;
			continue;
		}
		if(!(uPrimevPrime || uPrimev || vPrimeu)){
			count[1]++;
			continue;
		}
	}
	fourCounts[1]=((double)count[1]/(double)k)*((double)W);
	fourCounts[2]=((double)count[2]/(double)k)*((double)W/(double)(2));
	fourCounts[3]=((double)count[3]/(double)k)*((double)W/(double)(4));
	fourCounts[4]=((double)count[4]/(double)k)*((double)W/(double)(6));
	fourCounts[5]=((double)count[5]/(double)k)*((double)W/(double)(12));
	double N1=0;
	for(int i=0;i<num_of_vertices;i++){
		int deg = cg->degree(i);
		N1+=((double)((deg)*(deg-1)*(deg-2))/(6.0));
	}
	fourCounts[0]=N1-fourCounts[2]-2.0*fourCounts[4]-4.0*fourCounts[5];
}


EdgeInfo* EdgeSample(CGraph *cg, int k){
	cg->sortById();
	EdgeIdx num_of_edges = (long)cg->nEdges;
	EdgeIdx ***sampledEdges; // k sampled edges, 3 sets of 2 vertices per sample
	//sampledEdges = EdgeSample(cg, k);
	// EdgeIdx Te [k];
	for (EdgeIdx i=0;i<num_of_edges;i++){
		printf("\n%ld\n",cg->nbors[i]);
	}

	// EdgeIdx *RandomEdge;
	// RandomEdge = KRandomEdgeSamples(cg, k);
	// //write above function

	// EdgeIdx u1;
	// EdgeIdx v1;

	// for(EdgeIdx i=0;i<k;i++){
	// 	EdgeIdx
	// }
}
#endif