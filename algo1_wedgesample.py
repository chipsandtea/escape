
# Algorithm 1: C-wedge sampler
# 1: Computer p_v for all vertices
# 2: Select k random vertices (with replacement) according to probability distribution defined by {p_v}.
# 3: For each selected vertex v, choose a uniform random pair of neighbors of v to generate a wedge.
# 4: Output fraction of closed wedges as estimate of C.

# Assuming we have an array v of vertices and an array of arrays N of neighbors of vertices:
W = 0 # total number of wedges
n = v.length # number of vertices
Wv = [] # number of wedges centered at vertex v

# == Counts number of wedges ==
# for all vertices
for i=1 -> n:
	#Wv = deg(v) choose 2 
	Wv[i]=(N[v[i]].length) choose 2
	#W=sum of all Wv's
	W += Wv[i]

#once W is calculated, Wv becomes the PDF of the probability distribution {p_v}
T=[] # and T becomes the CDF of {p_v}
Wv[1]=W[1]/W
T[1]=W[1]
for i=2 -> n:
	Wv[i]=Wv[i]/W
	T[i]=T[i-1]+Wv[i]

#########################
# == 2: Select k random vertices (w/ replacement) according ==
# to probability distribution defined by {p_v}.
SelectK(k):
	# X is an array holding the values of the indices of selected random K vertices
	X=[]
	for i=1->k:
		# Set a = to some float b/w 0 and 1
		a=rand(0,1)
		
		# find the index corresponding to the random number, using the CDF
		for j=1->n:
			if T[j]>a and j is not in X:
				X.append(j)
				break

######################################
# == For each selected vertex v, choose a uniform random pair of neighbors of v to generate a wedge. ==
for i=1->X.length:
	#initialized closed wedges, total wedges
	closed=0
	total=0
	#M is the length of neighbors of the vertex X[i]
	M=N[v[X[i]]].length 
	if M=1:
		continue
	#select 2 vertices randomly from the list of neighbors of the vertex at index X[i]
	v1=v[N[v[X[i]]][ceil(rand(0,M)/M)]]
	v2=v[N[v[X[i]]][ceil(rand(0,M)/M)]]
	#if v2 is the same vertex as v1, select a new vertex as v2. Worst case scenario, the expected number of times to do this is once in the case of 2 neighbors
	while v2==v1:
		v[N[v[X[i]]][ceil(rand(0,M)/M)]]
	if (v1,v2) is an edge:
		closed++
	total++

return closed/total