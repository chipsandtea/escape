#include <string>
#include <fstream>
#include "Escape/GraphIO.h"
#include "Escape/EdgeHash.h"
#include "Escape/Digraph.h"
#include "Escape/Triadic.h"
#include "Escape/Graph.h"
#include "Escape/PathSampling.h"
#include "Escape/ThreeSample.h"

using namespace Escape;

int main(int argc, char *argv[])
{

  printf("main");
  Graph g;
  if (loadGraph(argv[1], g, 1, IOFormat::escape))
    exit(1);

  printf("Loaded graph\n");
  CGraph cg = makeCSR(g);
  cg.sortById();
  printf("Converted to CSR\n");

  printf("Relabeling graph\n");
  CGraph cg_relabel = cg.renameByDegreeOrder();
  cg_relabel.sortById();
  
  printf("Creating DAG\n");
  CDAG dag = degreeOrdered(&cg_relabel);

  (dag.outlist).sortById();
  (dag.inlist).sortById();

  int numToSample = std::stoll(argv[2]);
  std::ofstream outfile;
  outfile.open("out.txt", std::ios_base::app);

  srand(time(NULL));
  
  double fourCounts[6];
  threePathSample(&cg_relabel, &dag, fourCounts, numToSample);
  printf("\ni = 1, 3-star: %f", fourCounts[0]);
  printf("\ni = 2, 3-path: %f", fourCounts[1]);
  printf("\ni = 3, tailed-triangle: %f", fourCounts[2]);
  //printf("\n%f", fourCounts[3]);
  printf("\ni = 4, 4-cycle: %f", fourCounts[3]);
  printf("\ni = 5, chordal-4-cycle: %f", fourCounts[4]);
  printf("\ni = 6, 4-clique: %f", fourCounts[5]);

  outfile << std::fixed << std::showpoint;
  outfile << std::setprecision(20);

  for(int i = 0; i < 6; i++){
    outfile << fourCounts[i];
    outfile << "\n";
  }
  outfile.close();
/*
  for(int i=0;i<6;i++){
    printf("\ni=%d, fourCounts[i]=%f\n",i,fourCounts[i]);
  }
*/
  // printf("\n NumEdges: %d \n", cg_relabel.nEdges);
  // EdgeIdx*** Test;
  // Test=threeEdgeSample(&cg_relabel, numToSample);
  // for(int i=0;i<numToSample;i++){
  //   printf("\ni=%ld, (u',u)=(%ld,%ld)\n",i,Test[i][0][0],Test[i][0][1]);

  //   printf("\n\t(u,v)=(%ld,%ld)\n",Test[i][1][0],Test[i][1][1]);

  //   printf("\n\t(v,v')=(%ld,%ld)\n",Test[i][2][0],Test[i][2][1]);
  // }
  /*
  for(int i = 0; i < std::stoi(argv[3]); i++){
    outfile << EdgeSample(&cg_relabel, numToSample);
    outfile << " "; 
    outfile << "\n";
  }
  outfile.close();
  */
}
