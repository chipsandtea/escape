#include <string>
#include <fstream>
#include "Escape/GraphIO.h"
#include "Escape/EdgeHash.h"
#include "Escape/Digraph.h"
#include "Escape/Triadic.h"
#include "Escape/Graph.h"
#include "Escape/PathSampling.h"

using namespace Escape;

int main(int argc, char *argv[])
{

  printf("main");
  Graph g;
  if (loadGraph(argv[1], g, 1, IOFormat::escape))
    exit(1);

  //printf("Loaded graph\n");
  CGraph cg = makeCSR(g);
  cg.sortById();
  //printf("Converted to CSR\n");

  //printf("Relabeling graph\n");
  CGraph cg_relabel = cg.renameByDegreeOrder();
  cg_relabel.sortById();
  
  //printf("Creating DAG\n");
  CDAG dag = degreeOrdered(&cg_relabel);

  (dag.outlist).sortById();
  (dag.inlist).sortById();

  int numToSample = std::stoll(argv[2]);
/*
  if(dag.outlist.nVertices<numToSample){
    printf("too many sample vertices\n");
    return 0;
  }
  */

  // PATH SAMPLING
  std::ofstream outfile;
  outfile.open("sample_out.txt", std::ios_base::app);

  srand(time(NULL));
  
  for(int i = 0; i < std::stoi(argv[3]); i++){
    // printf("%d\n", numToSample);
    // Conditional jump or move depends on uninitialised value(s) here?
    outfile << WedgeSample(&cg_relabel, numToSample); 
    outfile << "\n";
  }
  //printf("done");
  outfile.close();
  
}
