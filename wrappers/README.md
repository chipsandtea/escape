# Escape Python Wrappers

First, to install the necessary external modules (matplotlib and numpy), run:
```
pip install -r requirements.txt
```

## Subgraph Count Python Wrapper (Enumeration)
### subgraph_counts.py
This script runs the enumeration algorithm on any `.edges` file. 

#### Execution

The default command to run this script on a graph takes the form:
```
python3 subgraph_counts.py ../graphs/<*.edges> <pattern size 3, 4> <-i, optional flag for integer values>
```

An example execution on the graph `ca-AstroPh.edges` would be:
```
python3 subgraph_counts.py ../graphs/ca-AstroPh.edges 3 -i
```

**Important:** When executing with the `-i` flag, the integer values are stored as a `.json` file in `../graphs/processed/<filename>.json`.

## Sampling Python Wrapper
### sampling_wrapper.py
This script runs the sampling algorithm described in [arXiv:1309.3321](https://arxiv.org/abs/1309.3321) for wedge sampling.

The algorithm for *3-path sampling* and *motif counting via sampling* described in [arXiv:1411.4942](https://arxiv.org/abs/1411.4942) are currently being tested or buggy.

#### Execution

The default command to run `sampling_wrapper.py` is:
```
python3 sampling_wrapper.py ../graphs/<*.edges> <pattern size 3> <# of samples per execution> <# of executions>
```

This should open a matplotlib window comparing the sampling average with the enumerated value.

**Important:** # of executions must be > 1, because only then will there be anything to plot.

For pattern == 3, this enumerated value = 3T/W, where **T** is the number of triangles, and **W** is the number of wedges.

#### How It Works
When you run the `sampling_wrapper.py` on a `.edges` file, it'll run `../exe/sample_three`, where the outputs are stored in `sample_out.txt`

To actually compare the sampled values with enumerated values, it calls `subgraph_counts.py` with the `-i` flag, where as stated above, stores these values in the directory `../graphs/processed/<filename>.json`. 

It then takes reads both the sampled and enumerated values, does some computation to find necessary values, and plots the enumerated value, the sampling outputs w/ respect to number of executions, and the sampling average.