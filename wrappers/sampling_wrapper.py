import subprocess, sys, os, json

# ../exe/PathSampling.h execution and output handling Python wrapper
# write to file pathsample.txt
# 
# Execution format: python3 sampling_wrapper.py <PATH FOR INPUT> <DESIRED PATTERN SIZE - 3> <NUMBER OF SAMPLES>
print(' SAMPLING _ WRAPPER ')
# Filepath of input file
inputPath = sys.argv[1]
# Execution pattern
pattern = int(sys.argv[2])
print('EXECUTION PATTERN: ' + sys.argv[2])
# Number of Samples
numOfSamples = sys.argv[3]
print('NUMBER OF SAMPLES: ' + numOfSamples)
# Number of Executions
try:
	numOfExecutions = sys.argv[4]
	flag = True
except:
	numOfExecutions = str(1)
	flag = False
print("NUMBER OF EXECUTIONS: " + numOfExecutions)

if len(sys.argv) < 3:
	print('Execution format: python3 sampling_wrapper.py <PATH FOR INPUT> <DESIRED PATTERN SIZE - 3> <NUMBER OF SAMPLES> <NUMBER OF EXECUTIONS>')

# if valid pattern size, execute according wrapper. quit if o/w
# subgraph_counts utilizes multiple executables per. 
if pattern == 3:
	#print(inputPath + str(numOfSamples))
	#print('../exe/sample_three ' + inputPath + ' ' + str(numOfSamples) + ' ' + numOfExecutions)
	#subprocess.run(['rm','pathsample.txt'])
	os.system("rm sample_out.txt")
	#os.system('../exe/sample_three ' + inputPath + ' ' + str(numOfSamples) + ' ' + numOfExecutions)
	print('calling now')
	subprocess.call(['../exe/sample_three', inputPath, numOfSamples, numOfExecutions])
	#subprocess.run(['../exe/sample_three', str(inputPath), str(numOfSamples)])
	
elif pattern == 4:
	subprocess.run(['../exe/sample_four/',inputPath, numOfSamples])
elif pattern == 5:
	subprocess.run(['../exe/<PATH SAMPLE WRAPPER 5>',inputPath, numOfSamples])
else:
	print('Incorrect format: Desired pattern size must be 3, 4, 5')
	sys.exit()


# Assume that post execution, <PATH SAMPLE WRAPPER> outputs a pathsample.txt
# w/ each line after with a sampling count output.
# Automatically execute subgraph_counts for enumeration outputs as well?
from matplotlib import pyplot as pp
import matplotlib.patches as mpatches
import numpy as np
numOfSamples = int(numOfSamples)
sampleRange = np.arange(1,numOfSamples,1)
sample = [] 
A=[]
tot=0;


avg=0;
# TO DO: Handling for 4 vertex executions and plotting against enumeration graph values.
with open('sample_out.txt', 'r') as samplingoutput:
# 	# TODO: add line = enumeration algorithm output on input graph
# 	# Add to subgraph_counts.py to output to a JSON.

	[sample.append(float(line)) for line in samplingoutput] #sample.append(result of wedgesample(cg,k,samplinoutput))

	print("NUM OF RECORDED OUTPUTS: " + str(len(sample)))
	if flag:
		for i in range(len(sample)):
			tot+=sample[i]
			A.append(tot/(i+1))
		q=len(A)
		try:
			with open('../graphs/processed/' + inputPath[10:-6] + '_processed.json') as enum_file:
				for line in enum_file:
					enum_dict = json.loads(line)
				#print(enum_dict['ca-AstroPh'])
			#for 3 pattern
		except:
			subprocess.call(['python3', 'subgraph_counts.py', inputPath, str(pattern), '-i'])
			with open('../graphs/processed/' + inputPath[10:-6] + '_processed.json') as enum_file:
				for line in enum_file:
					enum_dict = json.loads(line)
		if pattern == 3:
			T = int(enum_dict[inputPath[10:-6]]['3']['Triangle']['non-induced'])
			W = int(enum_dict[inputPath[10:-6]]['3']['Wedge']['non-induced'])
			C = 3 * T / W
			print('C = ' + str(C))
		elif pattern == 4:
			print('not implemented yet!')
			quit()
		elif pattern == 5:
			print('not implemented yet!')
		print("Difference = " + str(C-A[q-1]))
		print(A[q-1])
		pp.plot(range(len(sample)),A)
		pp.axhline(y=C, color='r')
		pp.axhline(y=A[q-1], color='g')
		percentage_error = ((A[q-1]-C)/(C))*100
		print('Percentage Error: ' + str(percentage_error) + "%")
		pp.xlabel('# of Executions')
		pp.ylabel('Estimates of C')
		red_patch = mpatches.Patch(color='red', label='Enumerated 3T/W')
		blue_patch = mpatches.Patch(color='blue', label='Sampling Outputs')
		green_patch = mpatches.Patch(color='green', label='Sampling Average')
		pp.legend(handles=[red_patch, blue_patch, green_patch])
		
		#pp.axhline(y=0.88, color='r')
		pp.show()
